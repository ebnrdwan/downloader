package ebnrdwan.app.loadinglibrary

import android.app.Application
import com.downloader.DownloaderConfiguration
import com.downloader.DownloaderObject



class DownloaderApp : Application() {

    override fun onCreate() {
        super.onCreate()
        val config = DownloaderConfiguration.Builder()
                .setDatabaseEnabled(true)
                .build()
        DownloaderObject.initialize(this, config)
    }

}
