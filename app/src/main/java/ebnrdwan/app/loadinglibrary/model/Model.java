package ebnrdwan.app.loadinglibrary.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public abstract class Model {

    @SerializedName("links")
    private LinksOfuser linksOfuser;
    @SerializedName("categories")
    private List<Categories> categories;
    @SerializedName("urls")
    private Urls urls;
    @SerializedName("current_user_collections")
    private List<String> current_user_collections;
    @SerializedName("user")
    private User user;
    @SerializedName("liked_by_user")
    private boolean liked_by_user;
    @SerializedName("likes")
    private int likes;
    @SerializedName("color")
    private String color;
    @SerializedName("height")
    private int height;
    @SerializedName("width")
    private int width;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("id")
    private String id;

    public LinksOfuser getLinksOfuser() {
        return linksOfuser;
    }

    public void setLinksOfuser(LinksOfuser linksOfuser) {
        this.linksOfuser = linksOfuser;
    }

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public Urls getUrls() {
        return urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    public List<String> getCurrent_user_collections() {
        return current_user_collections;
    }

    public void setCurrent_user_collections(List<String> current_user_collections) {
        this.current_user_collections = current_user_collections;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean getLiked_by_user() {
        return liked_by_user;
    }

    public void setLiked_by_user(boolean liked_by_user) {
        this.liked_by_user = liked_by_user;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class Links {
    }

    public static class Categories {
    }

    public static class Urls {
        @SerializedName("thumb")
        private String thumb;
        @SerializedName("small")
        private String small;
        @SerializedName("regular")
        private String regular;
        @SerializedName("full")
        private String full;
        @SerializedName("raw")
        private String raw;

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getSmall() {
            return small;
        }

        public void setSmall(String small) {
            this.small = small;
        }

        public String getRegular() {
            return regular;
        }

        public void setRegular(String regular) {
            this.regular = regular;
        }

        public String getFull() {
            return full;
        }

        public void setFull(String full) {
            this.full = full;
        }

        public String getRaw() {
            return raw;
        }

        public void setRaw(String raw) {
            this.raw = raw;
        }
    }

    public static class User {
        @SerializedName("linksOfuser")
        private LinksOfuser linksOfuser;
        @SerializedName("profile_image")
        private Profile_image profile_image;
        @SerializedName("name")
        private String name;
        @SerializedName("username")
        private String username;
        @SerializedName("id")
        private String id;

        public LinksOfuser getLinksOfuser() {
            return linksOfuser;
        }

        public void setLinksOfuser(LinksOfuser linksOfuser) {
            this.linksOfuser = linksOfuser;
        }

        public Profile_image getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(Profile_image profile_image) {
            this.profile_image = profile_image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class LinksOfuser {
        @SerializedName("likes")
        private String likes;
        @SerializedName("photos")
        private String photos;
        @SerializedName("html")
        private String html;
        @SerializedName("self")
        private String self;

        public String getLikes() {
            return likes;
        }

        public void setLikes(String likes) {
            this.likes = likes;
        }

        public String getPhotos() {
            return photos;
        }

        public void setPhotos(String photos) {
            this.photos = photos;
        }

        public String getHtml() {
            return html;
        }

        public void setHtml(String html) {
            this.html = html;
        }

        public String getSelf() {
            return self;
        }

        public void setSelf(String self) {
            this.self = self;
        }
    }

    public static class Profile_image {
        @SerializedName("large")
        private String large;
        @SerializedName("medium")
        private String medium;
        @SerializedName("small")
        private String small;

        public String getLarge() {
            return large;
        }

        public void setLarge(String large) {
            this.large = large;
        }

        public String getMedium() {
            return medium;
        }

        public void setMedium(String medium) {
            this.medium = medium;
        }

        public String getSmall() {
            return small;
        }

        public void setSmall(String small) {
            this.small = small;
        }
    }
}
