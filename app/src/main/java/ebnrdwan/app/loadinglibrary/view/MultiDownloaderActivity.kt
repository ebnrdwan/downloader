package ebnrdwan.app.loadinglibrary.view

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.downloader.*
import ebnrdwan.app.loadinglibrary.R
import ebnrdwan.app.loadinglibrary.utils.Utils
import kotlinx.android.synthetic.main.activity_multi_downloder.*

class MultiDownloaderActivity : AppCompatActivity() {

    internal val URL1 = "https://images.unsplash.com/photo-1464550883968-cec281c19761?ixlib=rb-0.3.5\\u0026q=80\\u0026fm=jpg\\u0026crop=entropy\\u0026s=4b142941bfd18159e2e4d166abcd0705"
    internal val URL2 = "http://www.appsapk.com/downloading/latest/WeChat-6.5.7.apk"
    internal val URL3 = "http://www.appsapk.com/downloading/latest/Instagram.apk"
    internal val URL4 = "http://www.appsapk.com/downloading/latest/Emoji%20Flashlight%20-%20Brightest%20Flashlight%202018-2.0.1.apk"



    internal var downloadIdOne: Int = 0
    internal var downloadIdTwo: Int = 0
    internal var downloadIdThree: Int = 0
    internal var downloadIdFour: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multi_downloder)

        dirPath = Utils.getRootDirPath(applicationContext)
        init()
    }

    private fun init() {
        onClickListenerOne()
        onClickListenerTwo()
        onClickListenerThree()
        onClickListenerFour()
    }

    fun onClickListenerOne() {
        buttonOne.setOnClickListener(View.OnClickListener {
            if (Status.RUNNING === DownloaderObject.getStatus(downloadIdOne)) {
                DownloaderObject.pause(downloadIdOne)
                return@OnClickListener
            }

            buttonOne.isEnabled = false
            progressBarOne.isIndeterminate = true
            progressBarOne.indeterminateDrawable.setColorFilter(
                    Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN)

            if (Status.PAUSED === DownloaderObject.getStatus(downloadIdOne)) {
                DownloaderObject.resume(downloadIdOne)
                return@OnClickListener
            }

            downloadIdOne = DownloaderObject.download(URL1, dirPath!!, "facebook.apk")
                    .build()
                    .setOnStartOrResumeListener(object : OnStartOrResumeListener {
                        override fun onStartOrResume() {
                            progressBarOne.isIndeterminate = false
                            buttonOne.isEnabled = true
                            buttonOne.setText(R.string.pause)
                            buttonCancelOne.isEnabled = true

                        }
                    })
                    .setOnPauseListener(object : OnPauseListener {
                        override fun onPause() {
                            buttonOne.setText(R.string.resume)
                        }
                    })
                    .setOnCancelListener(object : OnCancelListener {
                        override fun onCancel() {
                            buttonOne.setText(R.string.start)
                            buttonCancelOne.isEnabled = false
                            progressBarOne.progress = 0
                            textViewProgressOne.text = ""
                            downloadIdOne = 0
                            progressBarOne.isIndeterminate = false
                        }
                    })
                    .setOnProgressListener(object : OnProgressListener {
                        override fun onProgress(progress: Progress) {
                            val progressPercent = progress.currentBytes * 100 / progress.totalBytes
                            progressBarOne.progress = progressPercent.toInt()
                            textViewProgressOne.text = Utils.getProgressDisplayLine(progress.currentBytes, progress.totalBytes)
                            progressBarOne.isIndeterminate = false
                        }
                    })
                    .start(object : OnDownloadListener {
                        override fun onDownloadComplete() {
                            buttonOne.isEnabled = false
                            buttonCancelOne.isEnabled = false
                            buttonOne.setText(R.string.completed)
                        }

                        override fun onError(error: Error) {
                            buttonOne.setText(R.string.start)
                            Toast.makeText(applicationContext, getString(R.string.some_error_occurred) + " " + "1", Toast.LENGTH_SHORT).show()
                            textViewProgressOne.text = ""
                            progressBarOne.progress = 0
                            downloadIdOne = 0
                            buttonCancelOne.isEnabled = false
                            progressBarOne.isIndeterminate = false
                            buttonOne.isEnabled = true
                        }
                    })
        })

        buttonCancelOne.setOnClickListener { DownloaderObject.cancel(downloadIdOne) }
    }

    fun onClickListenerTwo() {
        buttonTwo.setOnClickListener(View.OnClickListener {
            if (Status.RUNNING === DownloaderObject.getStatus(downloadIdTwo)) {
                DownloaderObject.pause(downloadIdTwo)
                return@OnClickListener
            }

            buttonTwo.isEnabled = false
            progressBarTwo.isIndeterminate = true
            progressBarTwo.indeterminateDrawable.setColorFilter(
                    Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN)

            if (Status.PAUSED === DownloaderObject.getStatus(downloadIdTwo)) {
                DownloaderObject.resume(downloadIdTwo)
                return@OnClickListener
            }
            downloadIdTwo = DownloaderObject.download(URL2, dirPath!!, "wechat.apk")
                    .build()
                    .setOnStartOrResumeListener(object : OnStartOrResumeListener {
                        override fun onStartOrResume() {
                            progressBarTwo.isIndeterminate = false
                            buttonTwo.isEnabled = true
                            buttonTwo.setText(R.string.pause)
                            buttonCancelTwo.isEnabled = true
                            buttonCancelTwo.setText(R.string.cancel)
                        }
                    })
                    .setOnPauseListener(object : OnPauseListener {
                        override fun onPause() {
                            buttonTwo.setText(R.string.resume)
                        }
                    })
                    .setOnCancelListener(object : OnCancelListener {
                        override fun onCancel() {
                            downloadIdTwo = 0
                            buttonTwo.setText(R.string.start)
                            buttonCancelTwo.isEnabled = false
                            progressBarTwo.progress = 0
                            textViewProgressTwo.text = ""
                            progressBarTwo.isIndeterminate = false
                        }
                    })
                    .setOnProgressListener(object : OnProgressListener {
                        override fun onProgress(progress: Progress) {
                            val progressPercent = progress.currentBytes * 100 / progress.totalBytes
                            progressBarTwo.progress = progressPercent.toInt()
                            textViewProgressTwo.text = Utils.getProgressDisplayLine(progress.currentBytes, progress.totalBytes)
                        }
                    })
                    .start(object : OnDownloadListener {
                        override fun onDownloadComplete() {
                            buttonTwo.isEnabled = false
                            buttonCancelTwo.isEnabled = false
                            buttonTwo.setText(R.string.completed)
                        }

                        override fun onError(error: Error) {
                            buttonTwo.setText(R.string.start)
                            Toast.makeText(applicationContext, getString(R.string.some_error_occurred) + " " + "2", Toast.LENGTH_SHORT).show()
                            textViewProgressTwo.text = ""
                            progressBarTwo.progress = 0
                            downloadIdTwo = 0
                            buttonCancelTwo.isEnabled = false
                            progressBarTwo.isIndeterminate = false
                            buttonTwo.isEnabled = true
                        }
                    })
        })

        buttonCancelTwo.setOnClickListener { DownloaderObject.cancel(downloadIdTwo) }
    }

    fun onClickListenerThree() {
        buttonThree.setOnClickListener(View.OnClickListener {
            if (Status.RUNNING === DownloaderObject.getStatus(downloadIdThree)) {
                DownloaderObject.pause(downloadIdThree)
                return@OnClickListener
            }

            buttonThree.isEnabled = false
            progressBarThree.isIndeterminate = true
            progressBarThree.indeterminateDrawable.setColorFilter(
                    Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN)

            if (Status.PAUSED === DownloaderObject.getStatus(downloadIdThree)) {
                DownloaderObject.resume(downloadIdThree)
                return@OnClickListener
            }
            downloadIdThree = DownloaderObject.download(URL3, dirPath!!, "instagram.apk")
                    .build()
                    .setOnStartOrResumeListener(object : OnStartOrResumeListener {
                        override fun onStartOrResume() {
                            progressBarThree.isIndeterminate = false
                            buttonThree.isEnabled = true
                            buttonThree.setText(R.string.pause)
                            buttonCancelThree.isEnabled = true
                            buttonCancelThree.setText(R.string.cancel)
                        }
                    })
                    .setOnPauseListener(object : OnPauseListener {
                        override fun onPause() {
                            buttonThree.setText(R.string.resume)
                        }
                    })
                    .setOnCancelListener(object : OnCancelListener {
                        override fun onCancel() {
                            downloadIdThree = 0
                            buttonThree.setText(R.string.start)
                            buttonCancelThree.isEnabled = false
                            progressBarThree.progress = 0
                            textViewProgressThree.text = ""
                            progressBarThree.isIndeterminate = false
                        }
                    })
                    .setOnProgressListener(object : OnProgressListener {
                        override fun onProgress(progress: Progress) {
                            val progressPercent = progress.currentBytes * 100 / progress.totalBytes
                            progressBarThree.progress = progressPercent.toInt()
                            textViewProgressThree.text = Utils.getProgressDisplayLine(progress.currentBytes, progress.totalBytes)
                        }
                    })
                    .start(object : OnDownloadListener {
                        override fun onDownloadComplete() {
                            buttonThree.isEnabled = false
                            buttonCancelThree.isEnabled = false
                            buttonThree.setText(R.string.completed)
                        }

                        override fun onError(error: Error) {
                            buttonThree.setText(R.string.start)
                            Toast.makeText(applicationContext, getString(R.string.some_error_occurred) + " " + "3", Toast.LENGTH_SHORT).show()
                            textViewProgressThree.text = ""
                            progressBarThree.progress = 0
                            downloadIdThree = 0
                            buttonCancelThree.isEnabled = false
                            progressBarThree.isIndeterminate = false
                            buttonThree.isEnabled = true
                        }
                    })
        })

        buttonCancelThree.setOnClickListener { DownloaderObject.cancel(downloadIdThree) }
    }

    fun onClickListenerFour() {
        buttonFour.setOnClickListener(View.OnClickListener {
            if (Status.RUNNING === DownloaderObject.getStatus(downloadIdFour)) {
                DownloaderObject.pause(downloadIdFour)
                return@OnClickListener
            }

            buttonFour.isEnabled = false
            progressBarFour.isIndeterminate = true
            progressBarFour.indeterminateDrawable.setColorFilter(
                    Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN)

            if (Status.PAUSED === DownloaderObject.getStatus(downloadIdFour)) {
                DownloaderObject.resume(downloadIdFour)
                return@OnClickListener
            }
            downloadIdFour = DownloaderObject.download(URL4, dirPath!!, "flashlight.apk")
                    .build()
                    .setOnStartOrResumeListener(object : OnStartOrResumeListener {
                        override fun onStartOrResume() {
                            progressBarFour.isIndeterminate = false
                            buttonFour.isEnabled = true
                            buttonFour.setText(R.string.pause)
                            buttonCancelFour.isEnabled = true
                            buttonCancelFour.setText(R.string.cancel)
                        }
                    })
                    .setOnPauseListener(object : OnPauseListener {
                        override fun onPause() {
                            buttonFour.setText(R.string.resume)
                        }
                    })
                    .setOnCancelListener(object : OnCancelListener {
                        override fun onCancel() {
                            downloadIdFour = 0
                            buttonFour.setText(R.string.start)
                            buttonCancelFour.isEnabled = false
                            progressBarFour.progress = 0
                            textViewProgressFour.text = ""
                            progressBarFour.isIndeterminate = false
                        }
                    })
                    .setOnProgressListener(object : OnProgressListener {
                        override fun onProgress(progress: Progress) {
                            val progressPercent = progress.currentBytes * 100 / progress.totalBytes
                            progressBarFour.progress = progressPercent.toInt()
                            textViewProgressFour.text = Utils.getProgressDisplayLine(progress.currentBytes, progress.totalBytes)
                        }
                    })
                    .start(object : OnDownloadListener {
                        override fun onDownloadComplete() {
                            buttonFour.isEnabled = false
                            buttonCancelFour.isEnabled = false
                            buttonFour.setText(R.string.completed)
                        }

                        override fun onError(error: Error) {
                            buttonFour.setText(R.string.start)
                            Toast.makeText(applicationContext, getString(R.string.some_error_occurred) + " " + "4", Toast.LENGTH_SHORT).show()
                            textViewProgressFour.text = ""
                            progressBarFour.progress = 0
                            downloadIdFour = 0
                            buttonCancelFour.isEnabled = false
                            progressBarFour.isIndeterminate = false
                            buttonFour.isEnabled = true
                        }
                    })
        })

        buttonCancelFour.setOnClickListener { DownloaderObject.cancel(downloadIdFour) }
    }


    companion object {

        private var dirPath: String? = null
    }

}

