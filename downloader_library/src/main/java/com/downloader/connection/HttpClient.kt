
package com.downloader.connection

import com.downloader.request.DownloadRequest

import java.io.IOException
import java.io.InputStream

/**
 * Created by ebnrdwan on 13/11/18.
 */

interface HttpClient : Cloneable {

    val responseCode: Int

    val inputStream: InputStream

    val contentLength: Long

    public override fun clone(): HttpClient

    @Throws(IOException::class)
    fun connect(request: DownloadRequest)

    fun getResponseHeader(name: String): String

    fun close()

}
