

package com.downloader.connection

import com.downloader.Constants
import com.downloader.request.DownloadRequest
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLConnection
import java.util.*

/**
 * Created by ebnrdwan on 13/11/18.
 */

class DefaultConnector : HttpClient {

    private var connection: URLConnection? = null

    override val responseCode: Int
        @Throws(IOException::class)
        get() {
            var responseCode = 0
            if (connection is HttpURLConnection) {
                responseCode = (connection as HttpURLConnection).responseCode
            }
            return responseCode
        }

    override val inputStream: InputStream
        @Throws(IOException::class)
        get() = connection!!.getInputStream()

    override val contentLength: Long
        get() {
            val length = connection!!.getHeaderField("Content-Length")
            try {
                return java.lang.Long.parseLong(length)
            } catch (e: NumberFormatException) {
                return -1
            }

        }

    override fun clone(): HttpClient {
        return DefaultConnector()
    }

    @Throws(IOException::class)
    override fun connect(request: DownloadRequest) {
        connection = URL(request.url).openConnection()
        connection!!.readTimeout = request.readTimeout
        connection!!.connectTimeout = request.connectTimeout
        val range = String.format(Locale.ENGLISH,
                "bytes=%d-", request.downloadedBytes)
        connection!!.addRequestProperty(Constants.RANGE, range)
        connection!!.addRequestProperty(Constants.CLIENT, request.userAgent)
        addHeaders(request)
        connection!!.connect()
    }

    override fun getResponseHeader(name: String): String {
        return connection!!.getHeaderField(name)
    }

    override fun close() {
        // no operation
    }

    private fun addHeaders(request: DownloadRequest) {
        val headers = request.headers
        if (headers != null) {
            val entries = headers.entries
            for ((name, list) in entries) {

                    for (value in list) {
                        connection!!.addRequestProperty(name, value)
                    }

            }
        }
    }

}
