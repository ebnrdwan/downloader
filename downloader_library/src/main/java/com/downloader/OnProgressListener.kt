

package com.downloader

/**
 * Created by ebnrdwan on 13/11/18.
 */

interface OnProgressListener {

    fun onProgress(progress: Progress)

}
