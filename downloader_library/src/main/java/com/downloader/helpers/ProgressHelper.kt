
package com.downloader.helpers

import android.os.Handler
import android.os.Looper
import android.os.Message
import com.downloader.Constants
import com.downloader.OnProgressListener
import com.downloader.Progress

/**
 * Created by ebnrdwan on 13/11/18.
 */

class ProgressHelper(private val listener: OnProgressListener?) : Handler(Looper.getMainLooper()) {

    override fun handleMessage(msg: Message) {
        when (msg.what) {
            Constants.UPDATE -> if (listener != null) {
                val progress = msg.obj as Progress
                listener.onProgress(progress)
            }
            else -> super.handleMessage(msg)
        }
    }
}
