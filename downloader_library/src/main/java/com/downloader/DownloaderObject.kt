
package com.downloader

/**
 * Created by ebnrdwan on 12/11/18.
 */

import android.content.Context

import com.downloader.base.BaseClass
import com.downloader.operation.ComponentHolder
import com.downloader.operation.DownloadRequestQueue
import com.downloader.request.DownloadRequestBuilder
import com.downloader.utils.Utils


object DownloaderObject {

    @JvmOverloads
    @JvmStatic
    fun initialize(context: Context, configuration: DownloaderConfiguration = DownloaderConfiguration.newBuilder().build()) {
        ComponentHolder.instance.init(context, configuration)
        DownloadRequestQueue.initialize()
    }


    @JvmStatic
    fun download(url: String, dirPath: String, fileName: String): DownloadRequestBuilder {
        return DownloadRequestBuilder(url, dirPath, fileName)
    }

    @JvmStatic
    fun pause(downloadId: Int) {
        DownloadRequestQueue.getInstance().pause(downloadId)
    }

    @JvmStatic
    fun resume(downloadId: Int) {
        DownloadRequestQueue.getInstance().resume(downloadId)
    }


    @JvmStatic
    fun cancel(downloadId: Int) {
        DownloadRequestQueue.getInstance().cancel(downloadId)
    }


    @JvmStatic
    fun cancel(tag: Any) {
        DownloadRequestQueue.getInstance().cancel(tag)
    }


    @JvmStatic
    fun cancelAll() {
        DownloadRequestQueue.getInstance().cancelAll()
    }


    @JvmStatic
    fun getStatus(downloadId: Int): Status? {
        return DownloadRequestQueue.getInstance().getStatus(downloadId)
    }


    @JvmStatic
    fun cleanUp(days: Int) {
        Utils.deleteUnwantedModelsAndTempFiles(days)
    }

    @JvmStatic
    fun shutDown() {
        BaseClass.shutDown()
    }

}

