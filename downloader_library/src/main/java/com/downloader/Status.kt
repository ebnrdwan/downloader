
package com.downloader


// todo we can use sealed classes later, good for holding values
enum class Status {

    QUEUED,

    RUNNING,

    PAUSED,

    COMPLETED,

    CANCELLED,

    UNKNOWN

}
