

package com.downloader.request

import com.downloader.*
import com.downloader.base.BaseClass
import com.downloader.operation.ComponentHolder
import com.downloader.operation.DownloadRequestQueue
import com.downloader.operation.SynchronousCall
import com.downloader.utils.Utils
import java.util.*
import java.util.concurrent.Future

/**
 * Created by ebnrdwan on 13/11/18.
 */

class DownloadRequest internal constructor(builder: DownloadRequestBuilder) {

    var priority: Priority? = null
    var tag: Any? = null
    var url: String? = null
    var dirPath: String? = null
    var fileName: String? = null
    var sequenceNumber: Int = 0
    var future: Future<*>? = null
    var downloadedBytes: Long = 0
    var totalBytes: Long = 0
    var readTimeout: Int = 0
    var connectTimeout: Int = 0
    var userAgent: String? = null
        get() {
            if (field == null) {
                this.userAgent = ComponentHolder.instance.getUserAgent()
            }
            return field
        }
    internal var onProgressListener: OnProgressListener? = null
    internal var onDownloadListener: OnDownloadListener? = null
    internal var onStartOrResumeListener: OnStartOrResumeListener? = null
    internal var onPauseListener: OnPauseListener? = null
    internal var onCancelListener: OnCancelListener? = null
    var downloadId: Int = 0
    val headers: HashMap<String, List<String>>?
    var status: Status? = null

    private val readTimeoutFromConfig: Int
        get() = ComponentHolder.instance.getReadTimeout()

    private val connectTimeoutFromConfig: Int
        get() = ComponentHolder.instance.getConnectTimeout()

    init {
        this.url = builder.url
        this.dirPath = builder.dirPath
        this.fileName = builder.fileName
        this.headers = builder.headerMap
        this.priority = builder.priority
        this.tag = builder.tag
        this.readTimeout = if (builder.readTimeout != 0)
            builder.readTimeout
        else
            readTimeoutFromConfig
        this.connectTimeout = if (builder.connectTimeout != 0)
            builder.connectTimeout
        else
            connectTimeoutFromConfig
        this.userAgent = builder.userAgent
    }

    fun getOnProgressListener(): OnProgressListener? {
        return onProgressListener
    }

    fun setOnStartOrResumeListener(onStartOrResumeListener: OnStartOrResumeListener): DownloadRequest {
        this.onStartOrResumeListener = onStartOrResumeListener
        return this
    }

    fun setOnProgressListener(onProgressListener: OnProgressListener): DownloadRequest {
        this.onProgressListener = onProgressListener
        return this
    }

    fun setOnPauseListener(onPauseListener: OnPauseListener): DownloadRequest {
        this.onPauseListener = onPauseListener
        return this
    }

    fun setOnCancelListener(onCancelListener: OnCancelListener): DownloadRequest {
        this.onCancelListener = onCancelListener
        return this
    }

    fun start(onDownloadListener: OnDownloadListener): Int {
        this.onDownloadListener = onDownloadListener
        downloadId = Utils.getUniqueId(url!!, dirPath!!, fileName!!)
        DownloadRequestQueue.getInstance().addRequest(this)
        return downloadId
    }

    fun executeSync(): Response {
        downloadId = Utils.getUniqueId(url!!, dirPath!!, fileName!!)
        return SynchronousCall(this).execute()
    }

    fun deliverError(error: Error) {
        if (status != Status.CANCELLED) {
            BaseClass.getInstance().taskPoster.forMainThreadTasks()
                    .execute {
                        if (onDownloadListener != null) {
                            onDownloadListener!!.onError(error)
                        }
                        finish()
                    }
        }
    }

    fun deliverSuccess() {
        if (status != Status.CANCELLED) {
            status = Status.COMPLETED
            BaseClass.getInstance().taskPoster.forMainThreadTasks()
                    .execute {
                        if (onDownloadListener != null) {
                            onDownloadListener!!.onDownloadComplete()
                        }
                        finish()
                    }
        }
    }

    fun deliverStartEvent() {
        if (status != Status.CANCELLED) {
            BaseClass.getInstance().taskPoster.forMainThreadTasks()
                    .execute {
                        if (onStartOrResumeListener != null) {
                            onStartOrResumeListener!!.onStartOrResume()
                        }
                    }
        }
    }

    fun deliverPauseEvent() {
        if (status != Status.CANCELLED) {
            BaseClass.getInstance().taskPoster.forMainThreadTasks()
                    .execute {
                        if (onPauseListener != null) {
                            onPauseListener!!.onPause()
                        }
                    }
        }
    }

    private fun deliverCancelEvent() {
        BaseClass.getInstance().taskPoster.forMainThreadTasks()
                .execute {
                    if (onCancelListener != null) {
                        onCancelListener!!.onCancel()
                    }
                }
    }

    fun cancel() {
        status = Status.CANCELLED
        if (future != null) {
            future!!.cancel(true)
        }
        deliverCancelEvent()
        Utils.deleteTempFileAndDatabaseEntryInBackground(Utils.getTempPath(dirPath, fileName), downloadId)
    }

    private fun finish() {
        destroy()
        DownloadRequestQueue.getInstance().finish(this)
    }

    private fun destroy() {
        this.onProgressListener = null
        this.onDownloadListener = null
        this.onStartOrResumeListener = null
        this.onPauseListener = null
        this.onCancelListener = null
    }

}
