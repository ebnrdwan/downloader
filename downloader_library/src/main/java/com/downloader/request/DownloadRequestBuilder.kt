

package com.downloader.request

import com.downloader.Priority
import java.util.*

/**
 * Created by ebnrdwan on 13/11/18.
 */

class DownloadRequestBuilder(internal var url: String, internal var dirPath: String, internal var fileName: String) : RequestBuilder {
    internal var priority = Priority.MEDIUM
    internal var tag: Any? = null
    internal var readTimeout: Int = 0
    internal var connectTimeout: Int = 0
    internal var userAgent: String? = null
    internal var headerMap: HashMap<String, List<String>>? = null

    override fun setHeader(name: String, value: String): DownloadRequestBuilder {
        if (headerMap == null) {
            headerMap = HashMap()
        }
        var list: MutableList<String>? = headerMap!![name] as MutableList<String>?
        if (list == null) {
            list = ArrayList()
            headerMap!![name] = list
        }
        if (!list.contains(value)) {
            list.add(value)
        }
        return this
    }

    override fun setPriority(priority: Priority): DownloadRequestBuilder {
        this.priority = priority
        return this
    }

    override fun setTag(tag: Any): DownloadRequestBuilder {
        this.tag = tag
        return this
    }

    override fun setReadTimeout(readTimeout: Int): DownloadRequestBuilder {
        this.readTimeout = readTimeout
        return this
    }

    override fun setConnectTimeout(connectTimeout: Int): DownloadRequestBuilder {
        this.connectTimeout = connectTimeout
        return this
    }

    override fun setUserAgent(userAgent: String): DownloadRequestBuilder {
        this.userAgent = userAgent
        return this
    }

    fun build(): DownloadRequest {
        return DownloadRequest(this)
    }

}
