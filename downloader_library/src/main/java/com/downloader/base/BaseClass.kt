

package com.downloader.base

/**
 * Created by ebnrdwan on 13/11/18.
 */

class BaseClass private constructor() {
    val taskPoster: taskPoster

    init {
        this.taskPoster = ProcessProvider()
    }

    companion object {

        private var instance: BaseClass? = null

        fun getInstance(): BaseClass {
            if (instance == null) {
                synchronized(BaseClass::class.java) {
                    if (instance == null) {
                        instance = BaseClass()
                    }
                }
            }
            return instance!!
        }

        fun shutDown() {
            if (instance != null) {
                instance = null
            }
        }
    }
}
