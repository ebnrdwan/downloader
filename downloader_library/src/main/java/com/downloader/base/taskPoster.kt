

package com.downloader.base

import java.util.concurrent.Executor

/**
 * Created by ebnrdwan on 13/11/18.
 */

interface taskPoster {

    fun forDownloadTasks(): DownloadingProcessor

    fun forBackgroundTasks(): Executor

    fun forMainThreadTasks(): Executor

}
