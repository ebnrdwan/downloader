package com.downloader.base


import android.os.Process
import java.util.concurrent.ThreadFactory

/**
 * Created by ebnrdwan on 13/11/18.
 */

class PeriorityProvider internal constructor(private val mThreadPriority: Int) : ThreadFactory {

    override fun newThread(runnable: Runnable): Thread {
        val wrapperRunnable = Runnable {
            try {
                Process.setThreadPriority(mThreadPriority)
            } catch (ignored: Throwable) {

            }

            runnable.run()
        }
        return Thread(wrapperRunnable)
    }
}