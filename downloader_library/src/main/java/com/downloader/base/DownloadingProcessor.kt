
package com.downloader.base

import com.downloader.operation.DownloadRunnable
import java.util.concurrent.*

/**
 * Created by ebnrdwan on 13/11/18.
 */

class DownloadingProcessor internal constructor(maxNumThreads: Int, threadFactory: ThreadFactory) : ThreadPoolExecutor(maxNumThreads, maxNumThreads, 0, TimeUnit.MILLISECONDS, PriorityBlockingQueue(), threadFactory) {

    override fun submit(task: Runnable): Future<*> {
        val futureTask = ComparePeriorities(task as DownloadRunnable)
        execute(futureTask)
        return futureTask
    }
}
