
package com.downloader.base

import android.os.Handler
import android.os.Looper

import java.util.concurrent.Executor

/**
 * Created by ebnrdwan on 13/11/18.
 */

class MainThreadExecutor : Executor {

    private val handler = Handler(Looper.getMainLooper())

    override fun execute(runnable: Runnable) {
        handler.post(runnable)
    }
}
