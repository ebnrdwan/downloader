

package com.downloader.base

import android.os.Process
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Created by ebnrdwan on 13/11/18.
 */

class ProcessProvider internal constructor() : taskPoster {
    private val networkExecutor: DownloadingProcessor
    private val backgroundExecutor: Executor
    private val mainThreadExecutor: Executor

    init {
        val backgroundPriorityThreadFactory = PeriorityProvider(Process.THREAD_PRIORITY_BACKGROUND)
        networkExecutor = DownloadingProcessor(DEFAULT_MAX_NUM_THREADS, backgroundPriorityThreadFactory)
        backgroundExecutor = Executors.newSingleThreadExecutor()
        mainThreadExecutor = MainThreadExecutor()
    }

    override fun forDownloadTasks(): DownloadingProcessor {
        return networkExecutor
    }

    override fun forBackgroundTasks(): Executor {
        return backgroundExecutor
    }

    override fun forMainThreadTasks(): Executor {
        return mainThreadExecutor
    }

    companion object {

        private val DEFAULT_MAX_NUM_THREADS = 2 * Runtime.getRuntime().availableProcessors() + 1
    }
}
