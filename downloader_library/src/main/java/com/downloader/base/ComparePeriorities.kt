

package com.downloader.base

import com.downloader.operation.DownloadRunnable
import java.util.concurrent.FutureTask

/**
 * Created by ebnrdwan on 13/11/18.
 */

class ComparePeriorities internal constructor(private val runnable: DownloadRunnable) : FutureTask<DownloadRunnable>(runnable, null), Comparable<ComparePeriorities> {

    override fun compareTo(other: ComparePeriorities): Int {
        val p1 = runnable.priority
        val p2 = other.runnable.priority
        return if (p1 == p2) runnable.sequence - other.runnable.sequence else p2!!.ordinal - p1!!.ordinal
    }
}
