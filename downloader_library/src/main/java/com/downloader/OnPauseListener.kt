

package com.downloader

/**
 * Created by ebnrdwan on 13/11/18.
 */

interface OnPauseListener {

    fun onPause()

}
