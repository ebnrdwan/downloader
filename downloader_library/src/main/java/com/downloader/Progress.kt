

package com.downloader

import java.io.Serializable

/**
 * Created by ebnrdwan on 13/11/18.
 */

class Progress(var currentBytes: Long, var totalBytes: Long) : Serializable {

    override fun toString(): String {
        return "Progress{" +
                "currentBytes=" + currentBytes +
                ", totalBytes=" + totalBytes +
                '}'.toString()
    }
}
