

package com.downloader

import com.downloader.connection.DefaultConnector
import com.downloader.connection.HttpClient

/**
 * Created by ebnrdwan on 13/11/18.
 */

class DownloaderConfiguration private constructor(builder: Builder) {

    var readTimeout: Int = 0
    var connectTimeout: Int = 0
    var userAgent: String? = null
    var httpClient: HttpClient? = null
    var isDatabaseEnabled: Boolean = false

    init {
        this.readTimeout = builder.readTimeout
        this.connectTimeout = builder.connectTimeout
        this.userAgent = builder.userAgent
        this.httpClient = builder.httpClient
        this.isDatabaseEnabled = builder.databaseEnabled
    }

    class Builder {

        internal var readTimeout = Constants.DEFAuLT_READ_TIMEOUT
        internal var connectTimeout = Constants.DEFAULT_CONNECT_TIMEOUT_IN_MILLS
        internal var userAgent = Constants.DEFAULT_CLIENT
        internal var httpClient: HttpClient = DefaultConnector()
        internal var databaseEnabled = false

        fun setReadTimeout(readTimeout: Int): Builder {
            this.readTimeout = readTimeout
            return this
        }

        fun setConnectTimeout(connectTimeout: Int): Builder {
            this.connectTimeout = connectTimeout
            return this
        }

        fun setUserAgent(userAgent: String): Builder {
            this.userAgent = userAgent
            return this
        }

        fun setHttpClient(httpClient: HttpClient): Builder {
            this.httpClient = httpClient
            return this
        }

        fun setDatabaseEnabled(databaseEnabled: Boolean): Builder {
            this.databaseEnabled = databaseEnabled
            return this
        }

        fun build(): DownloaderConfiguration {
            return DownloaderConfiguration(this)
        }
    }

    companion object {

        fun newBuilder(): Builder {
            return Builder()
        }
    }
}
