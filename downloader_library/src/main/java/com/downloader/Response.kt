

package com.downloader

/**
 * Created by ebnrdwan on 13/11/18.
 */

class Response {

    var error: Error? = null
    var isSuccessful: Boolean = false
    var isPaused: Boolean = false
    var isCancelled: Boolean = false

}
