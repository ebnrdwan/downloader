

package com.downloader

/**
 * Created by ebnrdwan on 13/11/18.
 */

class Error {

    var isServerError: Boolean = false
    var isConnectionError: Boolean = false
}
