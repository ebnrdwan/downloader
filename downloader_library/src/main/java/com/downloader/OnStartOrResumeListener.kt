

package com.downloader


interface OnStartOrResumeListener {

    fun onStartOrResume()

}
