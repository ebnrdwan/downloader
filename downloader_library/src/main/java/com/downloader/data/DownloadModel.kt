

package com.downloader.data


class DownloadModel {

    var id: Int = 0
    var url: String? = null
    var eTag: String? = null
    var dirPath: String? = null
    var fileName: String? = null
    var totalBytes: Long = 0
    var downloadedBytes: Long = 0
    var lastModifiedAt: Long = 0

    companion object {

        internal val ID = "id"
        internal val URL = "url"
        internal val ETAG = "etag"
        internal val DIR_PATH = "dir_path"
        internal val FILE_NAME = "file_name"
        internal val TOTAL_BYTES = "total_bytes"
        internal val DOWNLOADED_BYTES = "downloaded_bytes"
        internal val LAST_MODIFIED_AT = "last_modified_at"
    }

}
