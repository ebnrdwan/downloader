/*
 *    Copyright (C) 2016 Amit Shekhar
 *    Copyright (C) 2011 Android Open Source Project
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.downloader.data

import android.annotation.SuppressLint
import java.util.*


open class LruCache<K, V>(private var maxSize: Int) {
    private val map: LinkedHashMap<K, V>

    private var size: Int = 0

    private var putCount: Int = 0
    private var createCount: Int = 0
    private var evictionCount: Int = 0
    private var hitCount: Int = 0
    private var missCount: Int = 0

    init {
        if (maxSize <= 0) {
            throw IllegalArgumentException("maxSize <= 0")
        }
        this.map = LinkedHashMap(0, 0.75f, true)
    }

    fun resize(maxSize: Int) {
        if (maxSize <= 0) {
            throw IllegalArgumentException("maxSize <= 0")
        }

        synchronized(this) {
            this.maxSize = maxSize
        }
        trimToSize(maxSize)
    }

    operator fun get(key: K?): V? {
        if (key == null) {
            throw NullPointerException("key == null")
        }

        var mapValue: V? = null
        synchronized(this) {
            mapValue = map[key]
            if (mapValue != null) {
                hitCount++
                return mapValue
            }
            missCount++
        }

        val createdValue = create(key) ?: return null

        synchronized(this) {
            createCount++
            mapValue = map.put(key, createdValue)

            if (mapValue != null) {
                // There was a conflict so undo that last put
                map.put(key, mapValue!!)
            } else {
                size += safeSizeOf(key, createdValue)
            }
        }

        if (mapValue != null) {
            entryRemoved(false, key, createdValue, mapValue)
            return mapValue
        } else {
            trimToSize(maxSize)
            return createdValue
        }
    }

    fun put(key: K?, value: V?): V? {
        if (key == null || value == null) {
            throw NullPointerException("key == null || value == null")
        }

        var previous: V? = null
        synchronized(this) {
            putCount++
            size += safeSizeOf(key, value)
            previous = map.put(key, value)
            if (previous != null) {
                size -= safeSizeOf(key, previous!!)
            }
        }

        if (previous != null) {
            entryRemoved(false, key, previous!!, value)
        }

        trimToSize(maxSize)
        return previous
    }

    fun trimToSize(maxSize: Int) {
        while (true) {
            var key: K? = null
            var value: V? = null
            synchronized(this) {
                if (size < 0 || map.isEmpty() && size != 0) {
                    throw IllegalStateException(javaClass.name + ".sizeOf() is reporting inconsistent results!")
                }

                if (size <= maxSize || map.isEmpty()) {
                    return
                }

                val toEvict = map.entries.iterator().next()
                key = toEvict.key
                value = toEvict.value
                map.remove(key!!)
                size -= safeSizeOf(key!!, value!!)
                evictionCount++
            }

            entryRemoved(true, key!!, value!!, null)
        }
    }

    fun remove(key: K?): V? {
        if (key == null) {
            throw NullPointerException("key == null")
        }

        var previous: V?=null
        synchronized(this) {
            previous = map.remove(key)
            if (previous != null) {
                size -= safeSizeOf(key, previous!!)
            }
        }

        if (previous != null) {
            entryRemoved(false, key, previous!!, null)
        }

        return previous
    }

    protected fun entryRemoved(evicted: Boolean, key: K, oldValue: V, newValue: V?) {}

    protected fun create(key: K): V? {
        return null
    }

    private fun safeSizeOf(key: K, value: V): Int {
        val result = sizeOf(key, value)
        if (result < 0) {
            throw IllegalStateException("Negative size: $key=$value")
        }
        return result
    }

    protected open fun sizeOf(key: K, value: V): Int {
        return 1
    }

    fun evictAll() {
        trimToSize(-1)
    }

    @Synchronized
    fun size(): Int {
        return size
    }

    @Synchronized
    fun maxSize(): Int {
        return maxSize
    }

    @Synchronized
    fun hitCount(): Int {
        return hitCount
    }

    @Synchronized
    fun missCount(): Int {
        return missCount
    }

    @Synchronized
    fun createCount(): Int {
        return createCount
    }

    @Synchronized
    fun putCount(): Int {
        return putCount
    }

    @Synchronized
    fun evictionCount(): Int {
        return evictionCount
    }

    @Synchronized
    fun snapshot(): Map<K, V> {
        return LinkedHashMap(map)
    }

    @SuppressLint("DefaultLocale")
    @Synchronized
    override fun toString(): String {
        val accesses = hitCount + missCount
        val hitPercent = if (accesses != 0) 100 * hitCount / accesses else 0
        return String.format("LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]",
                maxSize, hitCount, missCount, hitPercent)
    }
}
