package com.downloader.operation

import com.downloader.Response
import com.downloader.request.DownloadRequest

class SynchronousCall(val request: DownloadRequest) {

    fun execute(): Response {
        val downloadTask = DownloadProcess.create(request)
        return downloadTask.run()
    }

}
