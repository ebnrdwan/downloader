package com.downloader.operation

import android.content.Context
import com.downloader.Constants
import com.downloader.DownloaderConfiguration
import com.downloader.DownloaderObject
import com.downloader.connection.DefaultConnector
import com.downloader.connection.HttpClient
import com.downloader.data.AppDbHelper
import com.downloader.data.DbHelper
import com.downloader.data.NoOpsDbHelper


public class ComponentHolder {
    private var readTimeout: Int = 0
    private var connectTimeout: Int = 0
    private var userAgent: String? = null
    private var httpClient: HttpClient? = null
    private var dbHelper: DbHelper? = null

    fun init(context: Context, config: DownloaderConfiguration) {
        this.readTimeout = config.readTimeout
        this.connectTimeout = config.connectTimeout
        this.userAgent = config.userAgent
        this.httpClient = config.httpClient
        this.dbHelper = if (config.isDatabaseEnabled) AppDbHelper(context) else NoOpsDbHelper()
        if (config.isDatabaseEnabled) {
            DownloaderObject.cleanUp(30)
        }
    }

    fun getReadTimeout(): Int {
        if (readTimeout == 0) {
            synchronized(ComponentHolder::class.java) {
                if (readTimeout == 0) {
                    readTimeout = Constants.DEFAuLT_READ_TIMEOUT
                }
            }
        }
        return readTimeout
    }

    fun getConnectTimeout(): Int {
        if (connectTimeout == 0) {
            synchronized(ComponentHolder::class.java) {
                if (connectTimeout == 0) {
                    connectTimeout = Constants.DEFAULT_CONNECT_TIMEOUT_IN_MILLS
                }
            }
        }
        return connectTimeout
    }

    fun getUserAgent(): String {
        if (userAgent == null) {
            synchronized(ComponentHolder::class.java) {
                if (userAgent == null) {
                    userAgent = Constants.DEFAULT_CLIENT
                }
            }
        }
        return userAgent!!
    }

    fun getDbHelper(): DbHelper {
        if (dbHelper == null) {
            synchronized(ComponentHolder::class.java) {
                if (dbHelper == null) {
                    dbHelper = NoOpsDbHelper()
                }
            }
        }
        return dbHelper!!
    }

    fun getHttpClient(): HttpClient {
        if (httpClient == null) {
            synchronized(ComponentHolder::class.java) {
                if (httpClient == null) {
                    httpClient = DefaultConnector()
                }
            }
        }
        return httpClient!!.clone()
    }

    companion object {

        val instance = ComponentHolder()
    }

}
