package com.downloader.operation

import com.downloader.*
import com.downloader.data.DownloadModel
import com.downloader.helpers.ProgressHelper
import com.downloader.connection.HttpClient
import com.downloader.request.DownloadRequest
import com.downloader.utils.Utils
import java.io.*
import java.net.HttpURLConnection

/**
 * Created by ebnrdwan on 13/11/18.
 */

class DownloadProcess private constructor(private val request: DownloadRequest) {
    private var progressHelper: ProgressHelper? = null
    private var lastSyncTime: Long = 0
    private var lastSyncBytes: Long = 0
    private var inputStream: InputStream? = null
    private var httpClient: HttpClient? = null
    private var totalBytes: Long = 0
    private var responseCode: Int = 0
    private var eTag: String? = null
    private var isResumeSupported: Boolean = false
    private var tempPath: String? = null

    private val isSuccessful: Boolean
        get() = responseCode >= HttpURLConnection.HTTP_OK && responseCode < HttpURLConnection.HTTP_MULT_CHOICE

    private val downloadModelIfAlreadyPresentInDatabase: DownloadModel
        get() = ComponentHolder.instance.getDbHelper().find(request.downloadId)

    internal fun run(): Response {

        val response = Response()

        if (request.status == Status.CANCELLED) {
            response.isCancelled = true
            return response
        } else if (request.status == Status.PAUSED) {
            response.isPaused = true
            return response
        }

        var outputStream: BufferedOutputStream? = null

        var fileDescriptor: FileDescriptor? = null

        try {

            if (request.onProgressListener != null) {
                progressHelper = ProgressHelper(request.onProgressListener)
            }

            tempPath = Utils.getTempPath(request.dirPath, request.fileName)

            val file = File(tempPath!!)

            var model: DownloadModel? = downloadModelIfAlreadyPresentInDatabase

            if (model != null) {
                if (file.exists()) {
                    request.totalBytes = model.totalBytes
                    request.downloadedBytes = model.downloadedBytes
                } else {
                    removeNoMoreNeededModelFromDatabase()
                    request.downloadedBytes = 0
                    request.totalBytes = 0
                    model = null
                }
            }

            httpClient = ComponentHolder.instance.getHttpClient()

            httpClient!!.connect(request)

            if (request.status == Status.CANCELLED) {
                response.isCancelled = true
                return response
            } else if (request.status == Status.PAUSED) {
                response.isPaused = true
                return response
            }

            httpClient = Utils.getRedirectedConnectionIfAny(httpClient!!, request)

            responseCode = httpClient!!.responseCode

            eTag = httpClient!!.getResponseHeader(Constants.dTAG)

            if (checkIfFreshStartRequiredAndStart(model)) {
                model = null
            }

            if (!isSuccessful) {
                val error = Error()
                error.isServerError = true
                response.error = error
                return response
            }

            setResumeSupportedOrNot()

            totalBytes = request.totalBytes

            if (!isResumeSupported) {
                deleteTempFile()
            }

            if (totalBytes == 0L) {
                totalBytes = httpClient!!.contentLength
                request.totalBytes = totalBytes
            }

            if (isResumeSupported && model == null) {
                createAndInsertNewModel()
            }

            if (request.status == Status.CANCELLED) {
                response.isCancelled = true
                return response
            } else if (request.status == Status.PAUSED) {
                response.isPaused = true
                return response
            }

            request.deliverStartEvent()

            inputStream = httpClient!!.inputStream

            val buff = ByteArray(BUFFER_SIZE)

            if (!file.exists()) {
                if (file.parentFile != null && !file.parentFile.exists()) {
                    if (file.parentFile.mkdirs()) {

                        file.createNewFile()
                    }
                } else {

                    file.createNewFile()
                }
            }

            val randomAccess = RandomAccessFile(file, "rw")
            fileDescriptor = randomAccess.fd
            outputStream = BufferedOutputStream(FileOutputStream(randomAccess.fd))

            if (isResumeSupported && request.downloadedBytes != 0L) {
                randomAccess.seek(request.downloadedBytes)
            }

            if (request.status == Status.CANCELLED) {
                response.isCancelled = true
                return response
            } else if (request.status == Status.PAUSED) {
                response.isPaused = true
                return response
            }

            do {

                val byteCount = inputStream!!.read(buff, 0, BUFFER_SIZE)

                if (byteCount == -1) {
                    break
                }

                outputStream.write(buff, 0, byteCount)

                request.downloadedBytes = request.downloadedBytes + byteCount

                sendProgress()

                syncIfRequired(outputStream, fileDescriptor)

                if (request.status == Status.CANCELLED) {
                    response.isCancelled = true
                    return response
                } else if (request.status == Status.PAUSED) {
                    sync(outputStream, fileDescriptor!!)
                    response.isPaused = true
                    return response
                }

            } while (true)

            val path = Utils.getPath(request.dirPath, request.fileName)

            Utils.renameFileName(tempPath!!, path)

            response.isSuccessful = true

            if (isResumeSupported) {
                removeNoMoreNeededModelFromDatabase()
            }

        } catch (e: IOException) {
            if (!isResumeSupported) {
                deleteTempFile()
            }
            val error = Error()
            error.isConnectionError = true
            response.error = error
        } catch (e: IllegalAccessException) {
            if (!isResumeSupported) {
                deleteTempFile()
            }
            val error = Error()
            error.isConnectionError = true
            response.error = error
        } finally {
            closeAllSafely(outputStream, fileDescriptor)
        }

        return response
    }

    private fun deleteTempFile() {
        val file = File(tempPath!!)
        if (file.exists()) {

            file.delete()
        }
    }

    private fun setResumeSupportedOrNot() {
        isResumeSupported = responseCode == HttpURLConnection.HTTP_PARTIAL
    }

    @Throws(IOException::class, IllegalAccessException::class)
    private fun checkIfFreshStartRequiredAndStart(model: DownloadModel?): Boolean {
        if (responseCode == Constants.HTTP_RANGE_NOT_SATISFIABLE || isETagChanged(model)) {
            if (model != null) {
                removeNoMoreNeededModelFromDatabase()
            }
            deleteTempFile()
            request.downloadedBytes = 0
            request.totalBytes = 0
            httpClient = ComponentHolder.instance.getHttpClient()
            httpClient!!.connect(request)
            httpClient = Utils.getRedirectedConnectionIfAny(httpClient!!, request)
            responseCode = httpClient!!.responseCode
            return true
        }
        return false
    }

    private fun isETagChanged(model: DownloadModel?): Boolean {
        return !(eTag == null || model == null || model.eTag == null) && model.eTag != eTag
    }

    private fun createAndInsertNewModel() {
        val model = DownloadModel()
        model.id = request.downloadId
        model.url = request.url
        model.eTag = eTag
        model.dirPath = request.dirPath
        model.fileName = request.fileName
        model.downloadedBytes = request.downloadedBytes
        model.totalBytes = totalBytes
        model.lastModifiedAt = System.currentTimeMillis()
        ComponentHolder.instance.getDbHelper().insert(model)
    }

    private fun removeNoMoreNeededModelFromDatabase() {
        ComponentHolder.instance.getDbHelper().remove(request.downloadId)
    }

    private fun sendProgress() {
        if (request.status != Status.CANCELLED) {
            if (progressHelper != null) {
                progressHelper!!
                        .obtainMessage(Constants.UPDATE,
                                Progress(request.downloadedBytes,
                                        totalBytes)).sendToTarget()
            }
        }
    }

    @Throws(IOException::class)
    private fun syncIfRequired(outputStream: BufferedOutputStream, fileDescriptor: FileDescriptor) {
        val currentBytes = request.downloadedBytes
        val currentTime = System.currentTimeMillis()
        val bytesDelta = currentBytes - lastSyncBytes
        val timeDelta = currentTime - lastSyncTime
        if (bytesDelta > MIN_BYTES_FOR_SYNC && timeDelta > TIME_GAP_FOR_SYNC) {
            sync(outputStream, fileDescriptor)
            lastSyncBytes = currentBytes
            lastSyncTime = currentTime
        }
    }

    private fun sync(outputStream: BufferedOutputStream, fileDescriptor: FileDescriptor) {
        var success: Boolean
        try {
            outputStream.flush()
            fileDescriptor.sync()
            success = true
        } catch (e: IOException) {
            success = false
            e.printStackTrace()
        }

        if (success && isResumeSupported) {
            ComponentHolder.instance.getDbHelper()
                    .updateProgress(request.downloadId,
                            request.downloadedBytes,
                            System.currentTimeMillis())
        }

    }

    private fun closeAllSafely(outputStream: BufferedOutputStream?, fileDescriptor: FileDescriptor?) {
        if (httpClient != null) {
            try {
                httpClient!!.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        if (inputStream != null) {
            try {
                inputStream!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        try {
            if (outputStream != null) {
                try {
                    outputStream.flush()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
            if (fileDescriptor != null) {
                try {
                    fileDescriptor.sync()
                } catch (e: SyncFailedException) {
                    e.printStackTrace()
                }

            }
        } finally {
            if (outputStream != null)
                try {
                    outputStream.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

        }
    }

    companion object {

        private val BUFFER_SIZE = 1024 * 4
        private val TIME_GAP_FOR_SYNC: Long = 2000
        private val MIN_BYTES_FOR_SYNC: Long = 65536

        internal fun create(request: DownloadRequest): DownloadProcess {
            return DownloadProcess(request)
        }
    }

}
