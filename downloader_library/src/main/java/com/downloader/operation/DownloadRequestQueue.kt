
package com.downloader.operation

import com.downloader.Status
import com.downloader.base.BaseClass
import com.downloader.request.DownloadRequest
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by ebnrdwan on 13/11/18.
 */

class DownloadRequestQueue private constructor() {
    private val currentRequestMap: MutableMap<Int, DownloadRequest>
    private val sequenceGenerator: AtomicInteger

    private val sequenceNumber: Int
        get() = sequenceGenerator.incrementAndGet()

    init {
        currentRequestMap = ConcurrentHashMap()
        sequenceGenerator = AtomicInteger()
    }

    fun pause(downloadId: Int) {
        val request = currentRequestMap[downloadId]
        if (request != null) {
            request.status = Status.PAUSED
        }
    }

    fun resume(downloadId: Int) {
        val request = currentRequestMap[downloadId]
        if (request != null) {
            request.status = Status.QUEUED
            request.future = BaseClass.getInstance()
                    .taskPoster
                    .forDownloadTasks()
                    .submit(DownloadRunnable(request))
        }
    }

    private fun cancelAndRemoveFromMap(request: DownloadRequest?) {
        if (request != null) {
            request.cancel()
            currentRequestMap.remove(request.downloadId)
        }
    }

    fun cancel(downloadId: Int) {
        val request = currentRequestMap[downloadId]
        cancelAndRemoveFromMap(request)
    }

    fun cancel(tag: Any) {
        for ((_, request) in currentRequestMap) {
            if (request.tag is String && tag is String) {
                val tempRequestTag = request.tag as String
                if (tempRequestTag == tag) {
                    cancelAndRemoveFromMap(request)
                }
            } else if (request.tag == tag) {
                cancelAndRemoveFromMap(request)
            }
        }
    }

    fun cancelAll() {
        for ((_, request) in currentRequestMap) {
            cancelAndRemoveFromMap(request)
        }
    }

    fun getStatus(downloadId: Int): Status? {
        val request = currentRequestMap[downloadId]
        return if (request != null) {
            request.status
        } else Status.UNKNOWN
    }

    fun addRequest(request: DownloadRequest) {
        currentRequestMap[request.downloadId] = request
        request.status = Status.QUEUED
        request.sequenceNumber = sequenceNumber
        request.future = BaseClass.getInstance()
                .taskPoster
                .forDownloadTasks()
                .submit(DownloadRunnable(request))
    }

    fun finish(request: DownloadRequest) {
        currentRequestMap.remove(request.downloadId)
    }

    companion object {

        private var instance: DownloadRequestQueue? = null

        fun initialize() {
            getInstance()
        }

        fun getInstance(): DownloadRequestQueue {
            if (instance == null) {
                synchronized(DownloadRequestQueue::class.java) {
                    if (instance == null) {
                        instance = DownloadRequestQueue()
                    }
                }
            }
            return instance!!
        }
    }
}
