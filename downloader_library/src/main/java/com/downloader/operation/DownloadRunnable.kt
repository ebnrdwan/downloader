

package com.downloader.operation

import com.downloader.Error
import com.downloader.Priority
import com.downloader.Status
import com.downloader.request.DownloadRequest

/**
 * Created by ebnrdwan on 13/11/18.
 */

class DownloadRunnable internal constructor(val request: DownloadRequest) : Runnable {

    val priority: Priority?
    val sequence: Int

    init {
        this.priority = request.priority
        this.sequence = request.sequenceNumber
    }

    override fun run() {
        request.status = Status.RUNNING
        val downloadTask = DownloadProcess.create(request)
        val response = downloadTask.run()
        if (response.isSuccessful) {
            request.deliverSuccess()
        } else if (response.isPaused) {
            request.deliverPauseEvent()
        } else if (response.error != null) {
            request.deliverError(response.error!!)
        } else if (!response.isCancelled) {
            request.deliverError(Error())
        }
    }

}
