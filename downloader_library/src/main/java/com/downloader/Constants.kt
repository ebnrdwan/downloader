

package com.downloader

/**
 * Created by ebnrdwan on 13/11/18.
 */

object Constants {

    val UPDATE = 0x01
    val RANGE = "Range"
    val dTAG = "ETag"
    val CLIENT = "User-Agent"
    val DEFAULT_CLIENT = "DownloaderObject"
    val DEFAuLT_READ_TIMEOUT = 30000
    val DEFAULT_CONNECT_TIMEOUT_IN_MILLS = 30000
    val HTTP_RANGE_NOT_SATISFIABLE = 416
    val HTTP_TEMPORARY_REDIRECT = 307
    val HTTP_PERMANENT_REDIRECT = 308

}// no instance
